import React, { Component } from 'react'

import DailySchedule from 'react-daily-schedule'
import 'react-daily-schedule/dist/index.css'

const exclude = [
  { start: 0, end: 540 },
  { start: 1080, end: 1440 },
  { start: 840, end: 900 }
]

export default class DailyScheduleWrapper extends Component {
  constructor(props) {
    super(props)

    this.onMoveEvent = this.onMoveEvent.bind(this)

    this.state = {
      appointments: [
        { id: 0, start: 540, end: 600 },
        { id: 1, start: 660, end: 690 },
        { id: 2, start: 720, end: 750 },
        { id: 3, start: 780, end: 840 },
      ]
    }
  }

  onMoveEvent(data) {
    this.setState({
      appointments: this.state.appointments.map(appointment => {
        if (appointment.id === data.source.id) {
          const duration = data.source.end - data.source.start
          return { ...appointment, start: data.target.start, end: data.target.start + duration }
        }

        return appointment
      })
    })
  }

  addAppointment(start, end) {
    this.setState({
      appointments: [
        ...this.state.appointments,
        { start, end, id: this.state.appointments.length },
      ]
    });
  }

  render() {
    return (
      <DailySchedule
        excludeRanges={exclude}
        appointments={this.state.appointments}
        onDrop={this.onMoveEvent}
        BucketComponent={({ start, end }) => <button onClick={() => this.addAppointment(start, end)}>+</button>}
        EventComponent={({ id }) => <div>Appointment {id}</div>}
      />
    )
  }
}
