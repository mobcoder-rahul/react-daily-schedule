# react-daily-schedule

> react package to handle daily calendar schedules

[![NPM](https://img.shields.io/npm/v/react-daily-schedule.svg)](https://www.npmjs.com/package/react-daily-schedule) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-daily-schedule
```

## Usage
- Visit the [Storybook](https://react-daily-schedule.netlify.com)
- Explore the complete example

## License

MIT © [nicokant](https://github.com/nicokant)
